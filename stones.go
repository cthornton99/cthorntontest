package main

import (
	"fmt"
)

func main() {

	stones := []int{1, 1, 1, 2, 2, 2, 10, 3, 3, 4, 4, 6, 6, 5}

	fmt.Println(stonesForCrown(stones))

}

func stonesForCrown(stones []int) int {
	n := make(map[int][]int)
	m := make(map[int]int)
	var sm []int
	//	var sn []int
	slen := len(stones)
	if slen > 100000 {
		return -1
	}
	for _, v := range stones {
		if v > 1000 {
			return -1
		}
		m[v] += 1
		if m[v] == 1 {
			sm = append(sm, v)
		}
	}

	for i, v := range m {
		n[v] = append(n[v], i)
	}

	fmt.Println("len = ", len(n))

	sz := 0
	rs := 0
	for i, v := range n {
		fmt.Println(v, "  ", i)
		if i > sz && len(v) > 1 {
			sz = i
			rs = 0
			for _, v := range v {
				if v > rs {
					rs = v
				}
			}
			fmt.Println("t= ", rs)
		}
	}
	if rs > 0 {
		return rs
	}
	return -1
}
